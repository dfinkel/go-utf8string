// Copyright 2022 David Finkel
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// Package utf8string provides types and helpers (currently just one String type)
// designed for making certain uses of unicode strings more efficient.
//
// The String type stores a UTF-8 encoded string as well as a memory-efficient
// slice of exceptions to the most common encoded code-point size.
//
// Other types may be added at a later date.
package utf8string

import (
	"unicode/utf8"
)

// String is the main type of the utf8string package, it represents an
// indexed utf8 string with O(log(m)) slicing by rune offset where m is the
// number of runes with encodings requiring an uncommon number of runes.
type String struct {
	inner     string
	offsets   offsetMgr        // offsets of runes with uncommon encoding-lengths
	base      indexSet[uint64] // base offsets from slicing
	commonLen uint8            // defaults to 1, may be reset if another length reaches a plurality.
	// Number of 16-bit integers required per rune when encoded with utf-16
	commonU16Len uint8 // defaults to 1, may be 2 if a majority of runes require surogate pairs
}

const defaultLen = uint8(1)

const maxSelfU16Rune = 0x0ffff

func genUTF8StringIndex(in string, commonLen uint8) (String, [6]int) {
	switch len(in) {
	case 0:
		return String{
			inner:        "",
			offsets:      nil,
			commonLen:    1,
			commonU16Len: 1,
		}, [6]int{}
	case 1:
		return String{
			inner:        in,
			offsets:      nil,
			commonLen:    1,
			commonU16Len: 1,
		}, [6]int{}

	default:
	}

	// The UTF-16 surrogate-half splitting occurs when the codepoint value is >
	// 0x10000, which appends to coincide with the 3/4-byte split in UTF-8
	// https://en.wikipedia.org/wiki/UTF-8#Encoding
	// https://en.wikipedia.org/wiki/UTF-16#Code_points_from_U+010000_to_U+10FFFF
	commonU16Len := uint8(1)
	if commonLen == 4 {
		commonU16Len = 2
	}

	// most english text, json, etc will have zero multi-byte UTF-8 characters,
	// so we can let exponential growth of the offsets slice handle other cases.
	// Start with a nil slice since append will allocate the backing array if necessary.
	offsets := offsetMgr(nil)
	switch {
	case uint64(len(in)) > (1 << 32):
		offsets = &runeOffsetSlice[uint64]{}
	case uint64(len(in)) > (1 << 16):
		offsets = &runeOffsetSlice[uint32]{}
	case uint64(len(in)) > (1 << 8):
		offsets = &runeOffsetSlice[uint16]{}
	default:
		offsets = &runeOffsetSlice[uint8]{}
	}

	// histogram of run lengths, used to decide whether to re-evaluate
	// common-lengths
	runeLens := [6]int{}

	lastByteOffset := 0
	runeOffset := 0
	u16Offset := 0
	lastu16Len := 1
	for z, r := range in {
		runeLen := z - lastByteOffset
		if runeLen != 0 && uint8(runeLen) != commonLen {
			offsets.append(
				indexSet[uint64]{
					r:   uint64(runeOffset - 1),
					b:   uint64(lastByteOffset),
					u16: uint64(u16Offset - lastu16Len),
				})
		}
		runeLens[runeLen]++

		lastByteOffset = z
		runeOffset++

		u16Len := 1
		if r > maxSelfU16Rune {
			u16Len = 2
		}
		u16Offset += u16Len
		lastu16Len = u16Len
	}

	if runeOffset == 1 {
		lens := [6]int{}
		lens[len(in)]++
		commonU16Len = 1
		if len(in) == 4 {
			commonU16Len = 2
		}
		// there was only one rune after all
		return String{
			inner:        in,
			offsets:      nil,
			commonLen:    uint8(len(in)),
			commonU16Len: commonU16Len,
		}, lens
	}
	finalRuneLen := len(in) - lastByteOffset
	runeLens[finalRuneLen]++

	// Add the final rune to the index if necessary
	if finalRuneLen != int(commonLen) {
		u16Adj := 1
		if finalRuneLen == 4 {
			u16Adj = 2
		}
		offsets.append(
			indexSet[uint64]{
				r:   uint64(runeOffset - 1),
				b:   uint64(lastByteOffset),
				u16: uint64(u16Offset - u16Adj),
			})
	}

	if offsets.len() == 0 {
		offsets = nil
	}

	return String{
		inner:        in,
		offsets:      offsets,
		commonLen:    commonLen,
		commonU16Len: commonU16Len,
	}, runeLens
}

// NewString creates a UTF8String, including the appropriate index.
func NewString(in string) String {
	// Generate the index once, making a full pass over everything.
	u, runeLens := genUTF8StringIndex(in, defaultLen)

	// Now that we have a histogram of rune-lengths, find the most common one.
	mostCommonLen := u.commonLen
	mostCommonCount := 0
	for l, cnt := range runeLens[1:] {
		if cnt > mostCommonCount {
			mostCommonLen = uint8(l + 1)
			mostCommonCount = cnt
		}
	}
	if mostCommonLen == u.commonLen {
		// We got it right the first time! We're done!
		return u
	}

	// now that we know what the distribution of lengths is like, re-generate
	// the index
	u, _ = genUTF8StringIndex(in, mostCommonLen)
	return u
}

// String implements fmt.Stringer, returning the underlying string-segment.
func (u *String) String() string {
	return u.inner
}

// RuneLen returns the number of runes in the string, with a result equivalent to
// len([]rune(u.String())), (but in constant time).
func (u *String) RuneLen() int {
	if u.offsets == nil || u.offsets.len() < 1 {
		// simple string with uniform-length runes.
		return len(u.inner) / int(u.commonLen)
	}
	backException := u.offsets.back()
	be := backException.sub(u.base)
	_, l := utf8.DecodeRuneInString(u.inner[be.b:])
	tailBOff := l + int(be.b)
	tailBLen := len(u.inner) - tailBOff
	return int(be.r) + tailBLen/int(u.commonLen) + 1
}

func (u *String) adjRune(mk indexSet[uint64], roffset int) indexSet[uint64] {
	mk = mk.sub(u.base)
	// the returned keypoint is after the offset we're searching for
	if mk.r > uint64(roffset) {
		return indexSet[uint64]{
			r:   uint64(roffset),
			u16: uint64(roffset) * uint64(u.commonU16Len),
			b:   uint64(roffset) * uint64(u.commonLen),
		}
	} else if mk.r == uint64(roffset) {
		// exact hit on an uncommon-length rune
		return mk.u64()
	}
	nr, abnormalLen := utf8.DecodeRuneInString(u.inner[mk.b:])

	mk.b += uint64(abnormalLen)
	mk.r++
	mk.u16++
	if nr > maxSelfU16Rune {
		mk.u16++
	}

	// now, we're past the exception. We can just do some math :)

	rm := uint64(roffset - int(mk.r))
	mk.b += rm * uint64(u.commonLen)
	mk.r += rm
	mk.u16 += rm * uint64(u.commonU16Len)
	return mk
}

// SeekRune returns the offset in bytes into the contained string for that
// rune offset.
func (u *String) SeekRune(roffset int) Cursor {
	if u.offsets == nil || u.offsets.len() == 0 {
		return Cursor{
			Byte: roffset * int(u.commonLen),
			u:    u,
			Rune: roffset,
			U16:  roffset * int(u.commonU16Len),
		}
	}
	mk := u.offsets.runeMark(uint64(roffset) + u.base.r)
	adj := u.adjRune(mk, roffset)
	return adj.cursor(u)
}

func (u *String) adjByte(mk indexSet[uint64], boffset int) indexSet[uint64] {
	mk = mk.sub(u.base)
	// the returned keypoint is after the offset we're searching for
	if mk.b > uint64(boffset) {
		return indexSet[uint64]{
			r:   uint64(boffset) / uint64(u.commonLen),
			u16: uint64(boffset) / uint64(u.commonLen) * uint64(u.commonU16Len),
			b:   uint64(boffset),
		}
	} else if mk.b == uint64(boffset) {
		// exact hit on an uncommon-length rune
		return mk.u64()
	}
	nr, abnormalLen := utf8.DecodeRuneInString(u.inner[mk.b:])

	mk.b += uint64(abnormalLen)
	mk.r++
	mk.u16++
	if nr > maxSelfU16Rune {
		mk.u16++
	}

	// now, we're past the exception. We can just do some math :)

	rm := uint64(boffset - int(mk.b))
	mk.b += rm
	mk.r += rm / uint64(u.commonLen)
	mk.u16 += rm / uint64(u.commonLen) * uint64(u.commonU16Len)
	return mk
}

// SeekByte provides a Cursor for the specified Byte-offset
func (u *String) SeekByte(boffset int) Cursor {
	if u.offsets == nil || u.offsets.len() == 0 {
		return Cursor{
			Byte: boffset,
			u:    u,
			Rune: boffset / int(u.commonLen),
			U16:  boffset / int(u.commonLen) * int(u.commonU16Len),
		}
	}
	mk := u.offsets.byteMark(uint64(boffset) + u.base.b)
	adj := u.adjByte(mk, boffset)
	return adj.cursor(u)
}

func (u *String) adjUTF16(mk indexSet[uint64], u16offset int) indexSet[uint64] {
	mk = mk.sub(u.base)
	if mk.u16 > uint64(u16offset) {
		return indexSet[uint64]{
			b:   uint64(u16offset) / uint64(u.commonU16Len) * uint64(u.commonLen),
			r:   uint64(u16offset) / uint64(u.commonU16Len),
			u16: uint64(u16offset),
		}
	} else if mk.u16 == uint64(u16offset) {
		// exact hit on an uncommon-length rune
		return mk.u64()
	}
	nr, abnormalLen := utf8.DecodeRuneInString(u.inner[mk.b:])

	mk.b += uint64(abnormalLen)
	mk.r++
	mk.u16++
	if nr > maxSelfU16Rune {
		mk.u16++
	}

	// now, we're past the exception. We can just do some math :)

	rm := uint64(u16offset - int(mk.u16))
	mk.u16 += rm
	mk.b += rm / uint64(u.commonU16Len) * uint64(u.commonLen)
	mk.r += rm / uint64(u.commonU16Len)

	return mk
}

// SeekUTF16 returns a Cursor for the UTF-16 offset
// (useful in webassembly when interacting with JS strings)
func (u *String) SeekUTF16(u16offset int) Cursor {
	if u.offsets == nil || u.offsets.len() == 0 {
		return Cursor{
			Byte: u16offset / int(u.commonU16Len) * int(u.commonLen),
			u:    u,
			Rune: u16offset / int(u.commonU16Len),
			U16:  u16offset,
		}
	}
	mk := u.offsets.u16Mark(uint64(u16offset) + u.base.u16)
	adj := u.adjUTF16(mk, u16offset)
	return adj.cursor(u)
}

// SliceByte returns a substring of u on the interval [i, j) (matching the
// slice expression u.String()[i:j]), except the relevant components of the
// index are preserved.
func (u *String) SliceByte(i, j int) String {
	if i == j {
		return String{
			inner:        "",
			offsets:      nil,
			commonLen:    1,
			commonU16Len: 1,
		}
	}
	base := String{
		inner:        u.inner[i:j],
		offsets:      nil,
		commonLen:    u.commonLen,
		commonU16Len: u.commonU16Len,
	}
	if u.offsets == nil || u.offsets.len() == 0 {
		// no exceptions. We're done :)
		return base
	}

	newOffsets, mk := u.offsets.sliceByte(uint64(i)-u.base.b, uint64(j)-u.base.b)
	if newOffsets == nil || newOffsets.len() == 0 {
		// if there's nothing left, just return the base
		return base
	}
	base.base = u.adjByte(mk, i)
	base.offsets = newOffsets

	return base
}

// SliceRune returns a substring of u on the rune-interval [i, j) (matching
// the slice expression string([]rune(u.String())[i:j])), except the relevant
// components of the index are preserved.
func (u *String) SliceRune(i, j int) String {
	// TODO: eliminate the extra binary-searching this delegation entails
	ibyte, jbyte := u.SeekRune(i).Byte, u.SeekRune(j).Byte
	return u.SliceByte(ibyte, jbyte)
}

// SliceU16 returns a substring of u on the rune-interval [i, j) (matching
// the slice expression string([]rune(u.String())[i:j])), except the relevant
// components of the index are preserved.
func (u *String) SliceU16(i, j int) String {
	if i == j {
		return String{
			inner:        "",
			offsets:      nil,
			commonLen:    1,
			commonU16Len: 1,
		}
	}
	base := String{
		offsets:      nil,
		commonLen:    u.commonLen,
		commonU16Len: u.commonU16Len,
	}
	if u.offsets == nil {
		// no exceptions. We're done :)
		base.inner = u.inner[i/int(u.commonU16Len)*int(u.commonLen) : j/int(u.commonU16Len)*int(u.commonLen)]
		return base
	}

	newOffsets, mk := u.offsets.sliceU16(uint64(i)-u.base.u16, uint64(j)-u.base.u16)

	bsi := u.adjUTF16(mk, i)
	if newOffsets == nil || newOffsets.len() == 0 {
		bsj := u.adjUTF16(mk, j)
		// if there's nothing left, just return the base with simple slicing
		base.inner = u.inner[bsi.b:bsj.b]
		return base
	}
	base.base = bsi
	base.offsets = newOffsets

	backOffset := u.adjUTF16(newOffsets.back(), j)
	base.inner = u.inner[base.base.b:backOffset.b]

	return base
}
