package utf8string

import (
	"testing"
	"unicode/utf16"
	"unicode/utf8"

	"golang.org/x/exp/slices"
)

func TestUTF8StringConstruct(t *testing.T) {
	for _, itbl := range []struct {
		name string
		in   string
		// expected string, with the input string omitted (that'll be verified
		// separately)
		expected String
	}{
		{
			name: "empty",
			in:   "",
			expected: String{
				offsets:      nil,
				commonLen:    1,
				commonU16Len: 1,
			},
		},
		{
			name: "one_ascii",
			in:   "i",
			expected: String{
				offsets:      nil,
				commonLen:    1,
				commonU16Len: 1,
			},
		},
		{
			name: "two_ascii",
			in:   "ij",
			expected: String{
				offsets:      nil,
				commonLen:    1,
				commonU16Len: 1,
			},
		},
		{
			name: "one_two_byte_rune",
			in:   "Ω",
			expected: String{
				offsets:      nil,
				commonLen:    2,
				commonU16Len: 1,
			},
		},
		{
			name: "one_three_byte_rune",
			in:   "‰",
			expected: String{
				offsets:      nil,
				commonLen:    3,
				commonU16Len: 1,
			},
		},
		{
			name: "one_four_byte_rune",
			in:   "𝐪", // Mathematical bold small Q U+1D42A
			expected: String{
				offsets:      nil,
				commonLen:    4,
				commonU16Len: 2,
			},
		},
		{
			name: "one_two_byte_rune_after_one_ascii",
			in:   "aΩ",
			expected: String{
				offsets:      &runeOffsetSlice[uint8]{{r: 1, b: 1, u16: 1}},
				commonLen:    1,
				commonU16Len: 1,
			},
		},
		{
			name: "one_three_byte_rune_after_one_ascii",
			in:   "a‰",
			expected: String{
				offsets:      &runeOffsetSlice[uint8]{{r: 1, b: 1, u16: 1}},
				commonLen:    1,
				commonU16Len: 1,
			},
		},
		{
			name: "one_four_byte_rune_after_one_ascii",
			in:   "a𝐪", // Mathematical bold small Q U+1D42A
			expected: String{
				offsets:      &runeOffsetSlice[uint8]{{r: 1, b: 1, u16: 1}},
				commonLen:    1,
				commonU16Len: 1,
			},
		},
		{
			name: "one_two_byte_rune_before_one ascii",
			in:   "Ωi",
			expected: String{
				offsets:      &runeOffsetSlice[uint8]{{r: 0, b: 0, u16: 0}},
				commonLen:    1,
				commonU16Len: 1,
			},
		},
		{
			name: "one_three_byte_rune_before_one ascii",
			in:   "‰i",
			expected: String{
				offsets:      &runeOffsetSlice[uint8]{{r: 0, b: 0, u16: 0}},
				commonLen:    1,
				commonU16Len: 1,
			},
		},
		{
			name: "one_four_byte_rune_before_one ascii",
			in:   "𝐪i", // Mathematical bold small Q U+1D42A
			expected: String{
				offsets:      &runeOffsetSlice[uint8]{{r: 0, b: 0, u16: 0}},
				commonLen:    1,
				commonU16Len: 1,
			},
		},
		{
			name: "one_rune_each_len_desc",
			in:   "𝐪‰Ωi",
			expected: String{
				offsets:      &runeOffsetSlice[uint8]{{r: 0, b: 0, u16: 0}, {r: 1, b: 4, u16: 2}, {r: 2, b: 7, u16: 3}},
				commonLen:    1,
				commonU16Len: 1,
			},
		},
		{
			name: "eight_four_byte_rune_before_one ascii",
			in:   "𝐪𝐪𝐪𝐪𝐪𝐪𝐪𝐪i", // Mathematical bold small Q U+1D42A
			expected: String{
				offsets:      &runeOffsetSlice[uint8]{{r: 8, b: 32, u16: 16}},
				commonLen:    4,
				commonU16Len: 2,
			},
		},
		{
			name: "eight_three_byte_rune_before_one ascii",
			in:   "‰‰‰‰‰‰‰‰i",
			expected: String{
				offsets:      &runeOffsetSlice[uint8]{{r: 8, b: 24, u16: 8}},
				commonLen:    3,
				commonU16Len: 1,
			},
		},
		{
			name: "eight_four_byte_runes",
			in:   "𝐪𝐪𝐪𝐪𝐪𝐪𝐪𝐪", // Mathematical bold small Q U+1D42A
			expected: String{
				offsets:      nil,
				commonLen:    4,
				commonU16Len: 2,
			},
		},
		{
			name: "eight_three_byte_runes",
			in:   "‰‰‰‰‰‰‰‰",
			expected: String{
				offsets:      nil,
				commonLen:    3,
				commonU16Len: 1,
			},
		},
		{
			name: "alternating_4_1_byte_runes",
			in:   "𝐪0𝐪0",
			expected: String{
				offsets:      &runeOffsetSlice[uint8]{{r: 0, b: 0, u16: 0}, {r: 2, b: 5, u16: 3}},
				commonLen:    1,
				commonU16Len: 1,
			},
		},
		{
			name: "sandwiching_1_byte_in_4_byte_runes",
			in:   "𝐪000𝐪𝐪",
			expected: String{
				offsets:      &runeOffsetSlice[uint8]{{r: 0, b: 0, u16: 0}, {r: 4, b: 7, u16: 5}, {r: 5, b: 11, u16: 7}},
				commonLen:    1,
				commonU16Len: 1,
			},
		},
	} {
		tbl := itbl
		t.Run(tbl.name, func(t *testing.T) {
			u8str := NewString(tbl.in)

			if u8str.String() != tbl.in {
				t.Errorf("input string doesn't match inner value: got %q; expected %q",
					u8str.String(), tbl.in)
			}
			if u8str.commonLen != tbl.expected.commonLen {
				t.Errorf("unexpected commonLen: got %d; expected %d",
					u8str.commonLen, tbl.expected.commonLen)
			}
			if u8str.commonU16Len != tbl.expected.commonU16Len {
				t.Errorf("unexpected commonU16Len: got %d; expected %d",
					u8str.commonU16Len, tbl.expected.commonU16Len)
			}
			if (tbl.expected.offsets == nil) && (u8str.offsets != nil) {
				t.Fatalf("got non-nil offsets; expected nil offsets (got %+v)", u8str.offsets)
			}
			if (tbl.expected.offsets == nil) != (u8str.offsets == nil) {
				t.Fatalf("unexpected nil-state of offsets slice: got %+v; expected %+v",
					u8str.offsets, tbl.expected.offsets)
			} else if tbl.expected.offsets != nil && tbl.expected.offsets.len() != u8str.offsets.len() {
				t.Fatalf("unexpected length of offsets slice: got %d; expected %d (%+v vs %+v)",
					u8str.offsets.len(), tbl.expected.offsets.len(),
					u8str.offsets, tbl.expected.offsets)
			}
			t.Logf("offsets: %+v", u8str.offsets)
			switch os := tbl.expected.offsets.(type) {
			case nil:
				// nothing to check (we've already taken care of this above
			case *runeOffsetSlice[uint8]:
				for i, idxEntry := range *os {
					ios := u8str.offsets.(*runeOffsetSlice[uint8])
					if (*ios)[i] != idxEntry {
						t.Errorf("unexpected value at index offset %d: got %+v; expected %+v",
							i, ios, idxEntry)
					}
				}
			case *runeOffsetSlice[uint16]:
				for i, idxEntry := range *os {
					ios := u8str.offsets.(*runeOffsetSlice[uint16])
					if (*ios)[i] != idxEntry {
						t.Errorf("unexpected value at index offset %d: got %+v; expected %+v",
							i, ios, idxEntry)
					}
				}
			case *runeOffsetSlice[uint32]:
				for i, idxEntry := range *os {
					ios := u8str.offsets.(*runeOffsetSlice[uint32])
					if (*ios)[i] != idxEntry {
						t.Errorf("unexpected value at index offset %d: got %+v; expected %+v",
							i, ios, idxEntry)
					}
				}
			case *runeOffsetSlice[uint64]:
				for i, idxEntry := range *os {
					ios := u8str.offsets.(*runeOffsetSlice[uint64])
					if (*ios)[i] != idxEntry {
						t.Errorf("unexpected value at index offset %d: got %+v; expected %+v",
							i, ios, idxEntry)
					}
				}
			}

			rlen := len([]rune(tbl.in))
			if u8str.RuneLen() != rlen {
				t.Errorf("unexpected rune length: for %q got %d; want: %d",
					tbl.in, u8str.RuneLen(), rlen)
			}

			roffset := 0
			for z := range u8str.String() {
				rs := []rune(tbl.in[:z])
				expU16Off := len(utf16.Encode(rs))
				{
					cur := u8str.SeekRune(roffset)
					if cur.Byte != z {
						t.Errorf("unexpected byte offset for rune offset %d in %q; got %+v; want %d",
							roffset, tbl.in, cur, z)
					}
					if cur.Rune != roffset {
						t.Errorf("unexpected rune offset for rune offset %d in %q; got %+v; want %d",
							roffset, tbl.in, cur, roffset)
					}
					if cur.U16 != expU16Off {
						t.Errorf("unexpected UTF-16 offset for rune offset %d in %q; got %+v; want %d",
							roffset, tbl.in, cur, expU16Off)
					}
				}
				{
					cur := u8str.SeekByte(z)
					if cur.Byte != z {
						t.Errorf("unexpected byte offset for byte offset %d in %q; got %+v; want %d",
							z, tbl.in, cur, z)
					}
					if cur.Rune != roffset {
						t.Errorf("unexpected rune offset for byte offset %d in %q; got %+v; want %d",
							z, tbl.in, cur, roffset)
					}
					if cur.U16 != expU16Off {
						t.Errorf("unexpected UTF-16 offset for byte offset %d in %q; got %+v; want %d",
							z, tbl.in, cur, expU16Off)
					}
				}
				{
					cur := u8str.SeekUTF16(expU16Off)
					if cur.Byte != z {
						t.Errorf("unexpected byte offset for UTF-16 offset %d in %q; got %+v; want %d",
							expU16Off, tbl.in, cur, z)
					}
					if cur.Rune != roffset {
						t.Errorf("unexpected rune offset for UTF-16 offset %d in %q; got %+v; want %d",
							expU16Off, tbl.in, cur, roffset)
					}
					if cur.U16 != expU16Off {
						t.Errorf("unexpected UTF-16 offset for UTF-16 offset %d in %q; got %+v; want %d",
							expU16Off, tbl.in, cur, expU16Off)
					}
				}

				roffset++
			}
		})
	}
}

func TestUTF8StringSliceByteIdxCheck(t *testing.T) {
	for _, itbl := range []struct {
		name               string
		in                 string
		start, end         int
		expectedOffsetsLen int
		expBase            indexSet[uint64]
	}{
		{
			name:               "empty",
			in:                 "",
			end:                0,
			start:              0,
			expectedOffsetsLen: 0,
		},
		{
			name:               "one_ascii_full_slice",
			in:                 "i",
			end:                1,
			start:              0,
			expectedOffsetsLen: 0,
		},
		{
			name:               "one_ascii_empty",
			in:                 "i",
			end:                0,
			start:              0,
			expectedOffsetsLen: 0,
		},
		{
			name:               "two_ascii_full_slice",
			in:                 "ij",
			end:                2,
			start:              0,
			expectedOffsetsLen: 0,
		},
		{
			name:               "two_ascii_first_half",
			in:                 "ij",
			end:                1,
			start:              0,
			expectedOffsetsLen: 0,
		},
		{
			name:               "one_four_byte_rune_full",
			in:                 "𝐪", // Mathematical bold small Q U+1D42A
			end:                4,
			start:              0,
			expectedOffsetsLen: 0,
		},
		{
			name:               "one_two_byte_rune_after_one_ascii_first_char",
			in:                 "aΩ",
			end:                1,
			start:              0,
			expectedOffsetsLen: 0,
		},
		{
			name:               "one_two_byte_rune_after_one_ascii_final_rune",
			in:                 "aΩ",
			end:                3,
			start:              1,
			expectedOffsetsLen: 1,
			expBase: indexSet[uint64]{
				r:   1,
				u16: 1,
				b:   1,
			},
		},
		{
			name:               "one_two_byte_rune_before_one_ascii_first_char",
			in:                 "Ωa",
			end:                2,
			start:              0,
			expectedOffsetsLen: 1,
		},
		{
			name:               "one_two_byte_rune_before_one_ascii_final_rune",
			in:                 "Ωa",
			end:                3,
			start:              2,
			expectedOffsetsLen: 0,
			expBase: indexSet[uint64]{
				r:   0,
				u16: 0,
				b:   0,
			},
		},
		{
			name:               "one_four_byte_rune_from_longer_mixed_string",
			in:                 "aaa𝐪𝐪aaa",
			end:                7,
			start:              3,
			expectedOffsetsLen: 1,
			expBase: indexSet[uint64]{
				r:   3,
				u16: 3,
				b:   3,
			},
		},
		{
			name:               "alternating_4_1_byte_runes",
			in:                 "𝐪0𝐪0",
			end:                9,
			start:              4,
			expectedOffsetsLen: 1,
			expBase: indexSet[uint64]{
				r:   1,
				u16: 2,
				b:   4,
			},
		},
		{
			name:               "sandwiching_1_byte_in_4_byte_runes",
			in:                 "𝐪000𝐪𝐪",
			end:                11,
			start:              4,
			expectedOffsetsLen: 1,
			expBase: indexSet[uint64]{
				r:   1,
				u16: 2,
				b:   4,
			},
		},
		{
			name:               "sandwiching_1_byte_in_4_byte_runes",
			in:                 "𝐪𝐪00000𝐪𝐪𝐪",
			end:                17,
			start:              8,
			expectedOffsetsLen: 1,
			expBase: indexSet[uint64]{
				r:   2,
				u16: 4,
				b:   8,
			},
		},
	} {
		tbl := itbl
		t.Run(tbl.name, func(t *testing.T) {
			inu8Str := NewString(tbl.in)
			slicedStr := inu8Str.SliceByte(tbl.start, tbl.end)
			expectedOutStr := tbl.in[tbl.start:tbl.end]
			if slicedStr.String() != expectedOutStr {
				t.Errorf("input string doesn't match inner value: got %q; expected %q",
					slicedStr.String(), expectedOutStr)
			}
			if slicedStr.commonLen != inu8Str.commonLen {
				t.Errorf("unexpected commonLen: got %d; expected %d",
					slicedStr.commonLen, inu8Str.commonLen)
			}

			t.Logf("sliceStr: %+v", slicedStr)
			t.Logf("offsets: %+v", slicedStr.offsets)

			expectedSlice, _ := genUTF8StringIndex(expectedOutStr,
				inu8Str.commonLen)

			// we have a special-case optimization in genUTF8StringIndex for
			// rune-length-1 strings, compensate for this.
			if expectedSlice.RuneLen() == 1 && tbl.expectedOffsetsLen == 1 {
				expectedSlice.offsets = &runeOffsetSlice[uint8]{{r: 0, b: 0}}
				expectedSlice.commonLen = uint8(len(expectedOutStr))
			}

			if (expectedSlice.offsets == nil) && (slicedStr.offsets != nil) {
				t.Fatalf("got non-nil offsets(%+v); expected nil offsets",
					slicedStr.offsets)
			}
			if tbl.expBase != slicedStr.base {
				t.Errorf("unexpected base; got: %+v; expected %+v", slicedStr.base, tbl.expBase)
			}
			if expectedSlice.offsets != nil {
				if expectedSlice.offsets.len() != slicedStr.offsets.len() {
					t.Fatalf("unexpected length of offsets slice: got %d; expected %d (%+v vs %+v)",
						slicedStr.offsets.len(), expectedSlice.offsets.len(),
						slicedStr.offsets, expectedSlice.offsets)
				}
				if tbl.expectedOffsetsLen != int(slicedStr.offsets.len()) {
					t.Fatalf("unexpected length of offsets slice for %q: got %d (%+v); expected %d",
						slicedStr.String(), slicedStr.offsets.len(),
						slicedStr.offsets, tbl.expectedOffsetsLen,
					)
				}
				for i, idxEntry := range *expectedSlice.offsets.(*runeOffsetSlice[uint8]) {
					if (*slicedStr.offsets.(*runeOffsetSlice[uint8]))[i] != idxEntry.add(convISet[uint64, uint8](tbl.expBase)) {
						t.Errorf("unexpected value at index offset %d: got %+v; expected %+v",
							i, (*slicedStr.offsets.(*runeOffsetSlice[uint8]))[i], idxEntry)
					}
				}
			}

			rlen := utf8.RuneCountInString(expectedSlice.String())
			if slicedStr.RuneLen() != rlen {
				t.Errorf("unexpected rune length: for %q got %d; want: %d",
					slicedStr.String(), slicedStr.RuneLen(), rlen)
			}
		})
	}
}

func TestUTF8StringSliceRuneIdxCheck(t *testing.T) {
	for _, itbl := range []struct {
		name               string
		in                 string
		start, end         int
		expectedOffsetsLen int
		expBase            indexSet[uint64]
	}{
		{
			name:               "empty",
			in:                 "",
			end:                0,
			start:              0,
			expectedOffsetsLen: 0,
		},
		{
			name:               "one_ascii_full_slice",
			in:                 "i",
			end:                1,
			start:              0,
			expectedOffsetsLen: 0,
		},
		{
			name:               "one_ascii_empty",
			in:                 "i",
			end:                0,
			start:              0,
			expectedOffsetsLen: 0,
		},
		{
			name:               "two_ascii_full_slice",
			in:                 "ij",
			end:                2,
			start:              0,
			expectedOffsetsLen: 0,
		},
		{
			name:               "two_ascii_first_half",
			in:                 "ij",
			end:                1,
			start:              0,
			expectedOffsetsLen: 0,
		},
		{
			name:               "one_four_byte_rune_full",
			in:                 "𝐪", // Mathematical bold small Q U+1D42A
			end:                1,
			start:              0,
			expectedOffsetsLen: 0,
		},
		{
			name:               "one_two_byte_rune_after_one_ascii_first_char",
			in:                 "aΩ",
			end:                1,
			start:              0,
			expectedOffsetsLen: 0,
		},
		{
			name:               "one_two_byte_rune_after_one_ascii_final_rune",
			in:                 "aΩ",
			end:                2,
			start:              1,
			expectedOffsetsLen: 1,
			expBase: indexSet[uint64]{
				b:   1,
				r:   1,
				u16: 1,
			},
		},
		{
			name:               "one_two_byte_rune_before_one_ascii_first_char",
			in:                 "Ωa",
			end:                1,
			start:              0,
			expectedOffsetsLen: 1,
		},
		{
			name:               "one_two_byte_rune_before_one_ascii_final_rune",
			in:                 "Ωa",
			end:                2,
			start:              1,
			expectedOffsetsLen: 0,
		},
		{
			name:               "eight_four_byte_runes_last_three_runes",
			in:                 "𝐪𝐪𝐪𝐪𝐪𝐪𝐪𝐪", // Mathematical bold small Q U+1D42A
			end:                8,
			start:              5,
			expectedOffsetsLen: 0,
			expBase: indexSet[uint64]{
				b:   0,
				r:   0,
				u16: 0,
			},
		},
		{
			name:               "eight_four_byte_runes_middle_three_runes",
			in:                 "𝐪𝐪𝐪𝐪𝐪𝐪𝐪𝐪", // Mathematical bold small Q U+1D42A
			end:                5,
			start:              2,
			expectedOffsetsLen: 0,
			expBase: indexSet[uint64]{
				b:   0,
				r:   0,
				u16: 0,
			},
		},
		{
			name:               "two_four_byte_runes_surrounded_by_one_bytes_middle_three_runes",
			in:                 "aaa𝐪𝐪aaa", // Mathematical bold small Q U+1D42A
			end:                5,
			start:              2,
			expectedOffsetsLen: 2,
			expBase: indexSet[uint64]{
				b:   2,
				r:   2,
				u16: 2,
			},
		},
	} {
		tbl := itbl
		t.Run(tbl.name, func(t *testing.T) {
			inu8Str := NewString(tbl.in)
			slicedStr := inu8Str.SliceRune(tbl.start, tbl.end)
			expectedOutStr := string([]rune(tbl.in)[tbl.start:tbl.end])
			if slicedStr.String() != expectedOutStr {
				t.Errorf("input string doesn't match inner value: got %q; expected %q",
					slicedStr.String(), expectedOutStr)
			}
			if slicedStr.commonLen != inu8Str.commonLen {
				t.Errorf("unexpected commonLen: got %d; expected %d",
					slicedStr.commonLen, inu8Str.commonLen)
			}

			expectedSlice, _ := genUTF8StringIndex(expectedOutStr,
				inu8Str.commonLen)

			// we have a special-case optimization in genUTF8StringIndex for
			// rune-length-1 strings, compensate for this.
			if expectedSlice.RuneLen() == 1 && tbl.expectedOffsetsLen == 1 {
				expectedSlice.offsets = &runeOffsetSlice[uint8]{{r: 0, b: 0}}
				expectedSlice.commonLen = uint8(len(expectedOutStr))
			}

			if (expectedSlice.offsets == nil) && (slicedStr.offsets != nil) {
				t.Fatalf("got non-nil offsets(%+v); expected nil offsets",
					slicedStr.offsets)
			}
			if tbl.expBase != slicedStr.base {
				t.Errorf("unexpected base; got: %+v; expected %+v", slicedStr.base, tbl.expBase)
			}
			if expectedSlice.offsets != nil {
				if expectedSlice.offsets.len() != slicedStr.offsets.len() {
					t.Fatalf("unexpected length of offsets slice: got %d; expected %d (%+v vs %+v)",
						slicedStr.offsets.len(), expectedSlice.offsets.len(),
						slicedStr.offsets, expectedSlice.offsets)
				}
				if tbl.expectedOffsetsLen != int(slicedStr.offsets.len()) {
					t.Fatalf("unexpected length of offsets slice for %q: got %d (%+v); expected %d",
						slicedStr.String(), slicedStr.offsets.len(),
						slicedStr.offsets, tbl.expectedOffsetsLen,
					)
				}
				for i, idxEntry := range *expectedSlice.offsets.(*runeOffsetSlice[uint8]) {
					if (*slicedStr.offsets.(*runeOffsetSlice[uint8]))[i] != idxEntry.add(convISet[uint64, uint8](tbl.expBase)) {
						t.Errorf("unexpected value at index offset %d: got %+v; expected %+v",
							i, (*slicedStr.offsets.(*runeOffsetSlice[uint8]))[i], idxEntry)
					}
				}
			}

			rlen := utf8.RuneCountInString(expectedSlice.String())
			if slicedStr.RuneLen() != rlen {
				t.Errorf("unexpected rune length: for %q got %d; want: %d",
					slicedStr.String(), slicedStr.RuneLen(), rlen)
			}
		})
	}
}

func FuzzConstructSliceByte(f *testing.F) {
	for _, seed := range []struct {
		in         string
		start, end int
	}{
		{
			in:    "",
			end:   0,
			start: 0,
		},
		{
			in:    "i",
			end:   1,
			start: 0,
		},
		{
			in:    "i",
			end:   0,
			start: 0,
		},
		{
			in:    "ij",
			end:   2,
			start: 0,
		},
		{
			in:  "ij",
			end: 1,
		},
		{
			in:    "𝐪", // Mathematical bold small Q U+1D42A
			end:   4,
			start: 0,
		},
		{
			in:    "aΩ",
			end:   1,
			start: 0,
		},
		{
			in:    "aΩ",
			end:   3,
			start: 1,
		},
		{
			in:    "Ωa",
			end:   2,
			start: 0,
		},
		{
			in:    "Ωa",
			end:   3,
			start: 2,
		},
		{
			in:    "aaa𝐪𝐪aaa",
			end:   7,
			start: 3,
		},
		{
			in:    "𝐪0𝐪0",
			end:   9,
			start: 4,
		},
		{
			in:    "𝐪000𝐪𝐪",
			end:   11,
			start: 4,
		},
		{
			in:    "𝐪𝐪00000𝐪𝐪𝐪",
			end:   17,
			start: 8,
		},
	} {
		f.Add(seed.in, seed.start, seed.end)
	}

	f.Fuzz(func(t *testing.T, in string, start, end int) {
		inu8Str := NewString(in)
		if inu8Str.String() != in {
			t.Errorf("unexpected value for .String(); got %q; want %q",
				inu8Str.String(), in)
		}
		// compute rune and utf16 slices
		rs := []rune(in)
		if inu8Str.RuneLen() != len(rs) {
			t.Errorf("unexpected rune length: got %d; want %d", inu8Str.RuneLen(), len(rs))
		}

		if start < 0 {
			t.Skipf("invalid start (%d < 0)", start)
		}
		if start > len(in) {
			t.Skipf("invalid start (%d > len(%q) = %d)", start, in, len(in))
		}
		if start > end {
			t.Skipf("invalid start, end (%d, %d)", start, end)
		}
		if end > len(in) {
			t.Skipf("invalid end (%d > len(%q) = %d)", end, in, len(in))
		}

		slin := in[start:end]
		if !utf8.ValidString(slin) {
			t.Skipf("%q (%q[%d:%d]) is invalid UTF-8", slin, in, start, end)
		}

		rsSlice := []rune(slin)
		{
			slc := inu8Str.SliceByte(start, end)

			if !slices.Equal([]rune(slc.String()), rsSlice) {
				t.Errorf("mismatched rune-slices for byte-slice %d, %d of %q: got %v; want: %v",
					start, end, in, []rune(slc.String()), rsSlice)
			}
			if slc.String() != slin {
				t.Errorf("mismatched string for byte-slice %d, %d of %q: got %q; want: %q",
					start, end, in, slc.String(), slin)
			}
			if slc.RuneLen() != len(rsSlice) {
				t.Errorf("unexpected RuneLen for byte-slice (%d, %d) of %q = %q (exp %q): got %d; want: %d",
					start, end, in, slc.String(), slin, slc.RuneLen(), len(rsSlice))
			}
		}

	})
}

func FuzzConstructSliceRune(f *testing.F) {
	for _, seed := range []struct {
		in         string
		start, end int
	}{
		{
			in:    "",
			end:   0,
			start: 0,
		},
		{
			in:    "i",
			end:   1,
			start: 0,
		},
		{
			in:    "i",
			end:   0,
			start: 0,
		},
		{
			in:    "ij",
			end:   2,
			start: 0,
		},
		{
			in:    "ij",
			end:   1,
			start: 0,
		},
		{
			in:    "𝐪", // Mathematical bold small Q U+1D42A
			end:   1,
			start: 0,
		},
		{
			in:    "aΩ",
			end:   1,
			start: 0,
		},
		{
			in:    "aΩ",
			end:   2,
			start: 1,
		},
		{
			in:    "Ωa",
			end:   1,
			start: 0,
		},
		{
			in:    "Ωa",
			end:   2,
			start: 1,
		},
		{
			in:    "aaa𝐪𝐪aaa",
			end:   4,
			start: 3,
		},
		{
			in:    "𝐪0𝐪0",
			end:   3,
			start: 1,
		},
		{
			in:    "𝐪000𝐪𝐪",
			end:   5,
			start: 1,
		},
		{
			in:    "𝐪𝐪00000𝐪𝐪𝐪",
			end:   8,
			start: 2,
		},
	} {
		f.Add(seed.in, seed.start, seed.end)
	}

	f.Fuzz(func(t *testing.T, in string, start, end int) {
		inu8Str := NewString(in)
		if inu8Str.String() != in {
			t.Errorf("unexpected value for .String(); got %q; want %q",
				inu8Str.String(), in)
		}
		// compute rune and utf16 slices
		rs := []rune(in)
		if inu8Str.RuneLen() != len(rs) {
			t.Errorf("unexpected rune length: got %d; want %d", inu8Str.RuneLen(), len(rs))
		}

		if start < 0 {
			t.Skipf("invalid start (%d < 0)", start)
		}
		if start > len(rs) {
			t.Skipf("invalid start (%d > len([]rune(%q)) = %d)", start, in, len(rs))
		}
		if start > end {
			t.Skipf("invalid start, end (%d, %d)", start, end)
		}
		if end > len(rs) {
			t.Skipf("invalid end (%d > len([]rune(%q)) = %d)", end, in, len(rs))
		}

		if !utf8.ValidString(in) {
			t.Skipf("invalid utf8 string: %q", in)
		}

		rsSlice := rs[start:end]
		slin := string(rsSlice)
		{
			slc := inu8Str.SliceRune(start, end)

			if !slices.Equal([]rune(slc.String()), rsSlice) {
				t.Errorf("mismatched rune-slices for byte-slice %d, %d of %q: got %v; want: %v",
					start, end, in, []rune(slc.String()), rsSlice)
			}
			if slc.String() != slin {
				t.Errorf("mismatched string for byte-slice %d, %d of %q: got %q; want: %q",
					start, end, in, slc.String(), slin)
			}
			if slc.RuneLen() != len(rsSlice) {
				t.Errorf("unexpected RuneLen for byte-slice (%d, %d) of %q = %q (exp %q): got %d; want: %d",
					start, end, in, slc.String(), slin, slc.RuneLen(), len(rsSlice))
			}
		}
	})
}

func FuzzConstructSliceU16(f *testing.F) {
	for _, seed := range []struct {
		in         string
		start, end int
	}{
		{
			in:    "",
			end:   0,
			start: 0,
		},
		{
			in:    "i",
			end:   1,
			start: 0,
		},
		{
			in:    "i",
			end:   0,
			start: 0,
		},
		{
			in:    "ij",
			end:   2,
			start: 0,
		},
		{
			in:    "ij",
			end:   1,
			start: 0,
		},
		{
			in:    "𝐪", // Mathematical bold small Q U+1D42A
			end:   2,
			start: 0,
		},
		{
			in:    "aΩ",
			end:   1,
			start: 0,
		},
		{
			in:    "aΩ",
			end:   2,
			start: 1,
		},
		{
			in:    "Ωa",
			end:   1,
			start: 0,
		},
		{
			in:    "Ωa",
			end:   2,
			start: 1,
		},
		{
			in:    "aaa𝐪𝐪aaa",
			end:   5,
			start: 3,
		},
		{
			in:    "𝐪0𝐪0",
			end:   4,
			start: 1,
		},
		{
			in:    "𝐪000𝐪𝐪",
			end:   6,
			start: 1,
		},
		{
			in:    "𝐪𝐪00000𝐪𝐪𝐪",
			end:   9,
			start: 2,
		},
	} {
		f.Add(seed.in, seed.start, seed.end)
	}

	f.Fuzz(func(t *testing.T, in string, start, end int) {
		inu8Str := NewString(in)
		if inu8Str.String() != in {
			t.Errorf("unexpected value for .String(); got %q; want %q",
				inu8Str.String(), in)
		}
		rs := []rune(in)
		u16s := utf16.Encode(rs)

		if start < 0 {
			t.Skipf("invalid start (%d < 0)", start)
		}
		if start > len(u16s) {
			t.Skipf("invalid start (%d > len(utf16.Encode(%q)) = %d)", start, in, len(u16s))
		}
		if start > end {
			t.Skipf("invalid start, end (%d, %d)", start, end)
		}
		if end > len(rs) {
			t.Skipf("invalid end (%d > len(utf16.Encode(%q)) = %d)", end, in, len(u16s))
		}

		if !utf8.ValidString(in) {
			t.Skipf("invalid utf8 string: %q", in)
		}

		rsSlice := utf16.Decode(u16s[start:end])

		slin := string(rsSlice)

		for _, r := range slin {
			if r == '�' {
				t.Skipf("invalid utf8 rune in slice")
			}
		}
		if !utf8.ValidString(slin) {
			t.Skipf("invalid utf8 string after slicing: %q", slin)
		}
		{
			// compute utf16 slices
			slc := inu8Str.SliceU16(start, end)

			if !slices.Equal([]rune(slc.String()), rsSlice) {
				t.Errorf("mismatched rune-slices for UTF-16-slice %d, %d of %q: got %v; want: %v",
					start, end, in, []rune(slc.String()), rsSlice)
			}
			if slc.String() != slin {
				t.Errorf("mismatched string for UTF-16-slice %d, %d of %q: got %q; want: %q",
					start, end, in, slc.String(), slin)
			}
			if slc.RuneLen() != len(rsSlice) {
				t.Errorf("unexpected RuneLen for UTF-16-slice (%d, %d) of %q = %q (exp %q): got %d; want: %d",
					start, end, in, slc.String(), slin, slc.RuneLen(), len(rsSlice))
			}
		}
	})
}
