package utf8string

// Cursor is a cursor representing an offset within the parent string.
type Cursor struct {
	Rune int
	U16  int
	Byte int

	// back-pointer
	u *String
}
