// Copyright 2022 David Finkel
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package utf8string

import (
	"fmt"
	"math/bits"
	"unsafe"

	"golang.org/x/exp/slices"
)

type sizedUints interface {
	uint8 | uint16 | uint32 | uint64
}

type indexSet[T sizedUints] struct {
	r, u16, b T
}

func (i *indexSet[T]) sub(base indexSet[T]) indexSet[T] {
	return indexSet[T]{
		r:   i.r - base.r,
		u16: i.u16 - base.u16,
		b:   i.b - base.b,
	}
}

func (i *indexSet[T]) add(base indexSet[T]) indexSet[T] {
	return indexSet[T]{
		r:   i.r + base.r,
		u16: i.u16 + base.u16,
		b:   i.b + base.b,
	}
}

func (i *indexSet[T]) u64() indexSet[uint64] {
	return convISet[T, uint64](*i)
}

func (i *indexSet[T]) cursor(u *String) Cursor {
	return Cursor{
		Rune: int(i.r),
		U16:  int(i.u16),
		Byte: int(i.b),
		u:    u,
	}
}

func convISet[S, T sizedUints](src indexSet[S]) indexSet[T] {
	return indexSet[T]{
		r:   T(src.r),
		u16: T(src.u16),
		b:   T(src.b),
	}
}

type offsetMgr interface {
	append(indexSet[uint64])
	runeMark(target uint64) indexSet[uint64]
	byteMark(target uint64) indexSet[uint64]
	u16Mark(target uint64) indexSet[uint64]
	len() uint64
	back() indexSet[uint64]
	sliceByte(target, length uint64) (offsetMgr, indexSet[uint64])
	sliceU16(target, length uint64) (offsetMgr, indexSet[uint64])
}

type runeOffsetSlice[T sizedUints] []indexSet[T]

func (rs *runeOffsetSlice[T]) len() uint64 {
	return uint64(len(*rs))
}

// precondition: must have rs.len() > 0
func (rs *runeOffsetSlice[T]) back() indexSet[uint64] {
	return (*rs)[len(*rs)-1].u64()
}

func (rs *runeOffsetSlice[T]) append(av indexSet[uint64]) {
	bitLen := unsafe.Sizeof(T(0)) * 8
	if bits.Len64(av.r) > int(bitLen) {
		panic(fmt.Errorf("rune offset too large (%d -- needs %d bits) for data-size: %d bits",
			av.r, bits.Len64(av.r), bitLen))
	}
	if bits.Len64(av.b) > int(bitLen) {
		panic(fmt.Errorf("byte offset too large (%d -- needs %d bits) for data-size: %d bits",
			av.b, bits.Len64(av.b), bitLen))
	}
	if bits.Len64(av.u16) > int(bitLen) {
		panic(fmt.Errorf("utf16 offset too large (%d -- needs %d bits) for data-size: %d bits",
			av.u16, bits.Len64(av.u16), bitLen))
	}

	if av.r > av.u16 {
		panic(fmt.Errorf("rune offset %d larger than utf16 offset %d", av.r, av.u16))
	}
	if av.u16 > av.b {
		panic(fmt.Errorf("utf16 offset %d larger than byte offset %d", av.u16, av.b))
	}

	*rs = append(*rs, indexSet[T]{
		r:   T(av.r),
		u16: T(av.u16),
		b:   T(av.b),
	})
}

func (rs runeOffsetSlice[T]) slice(iaddr func(ent indexSet[T]) T, target, end uint64) (offsetMgr, indexSet[uint64]) {
	addr := func(ent indexSet[T]) uint64 {
		return uint64(iaddr(ent))
	}
	si, sIndex := rs.find(func(v indexSet[T]) bool {
		return addr(v) >= target
	})

	if addr(si) > uint64(target) {
		if sIndex == 0 {
			si = indexSet[T]{
				r:   0,
				u16: 0,
				b:   0,
			}
		} else {
			si = rs[sIndex-1]
		}

	} else if addr(si) < target {
		// not an exact match, throw out the previous entry
		sIndex++
	}

	if sIndex == len(rs) {
		// don't bother slicing if we're only dealing with the last entry
		return nil, si.u64()
	}
	rem := rs[sIndex:]

	if len(rem) == 0 {
		// sanity-check: make sure we don't bother with an empty slice
		// return nil, si
		panic("impossible: should have been guarded by sIndex == len(rs) check above")
	}

	// the first remaining entry is beyond the end of the new slice
	if addr(rem[0]) >= end {
		return nil, si.u64()
	}

	// there is no right-side truncation
	if end > addr(rem[len(rem)-1]) {
		return &rem, si.u64()
	}
	// find the end-mark
	remE := slices.BinarySearchFunc(rem, func(v indexSet[T]) bool {
		return addr(v) >= end
	})

	// there's nothing to save
	if remE == 0 {
		return nil, si.u64()
	}

	if addr(rem[remE-1]) >= end {
		remE--
	}

	rem = rem[:remE]

	return &rem, si.u64()
}

func (rs runeOffsetSlice[T]) sliceByte(target, end uint64) (offsetMgr, indexSet[uint64]) {
	return rs.slice(func(v indexSet[T]) T { return v.b }, target, end)
}

func (rs runeOffsetSlice[T]) sliceU16(target, end uint64) (offsetMgr, indexSet[uint64]) {
	return rs.slice(func(v indexSet[T]) T { return v.u16 }, target, end)
}

func (rs runeOffsetSlice[T]) find(ok func(v indexSet[T]) bool) (indexSet[T], int) {
	if len(rs) == 0 {
		return indexSet[T]{
			r:   0,
			u16: 0,
			b:   0,
		}, 0
	}
	o := slices.BinarySearchFunc(rs, ok)

	if o > len(rs)-1 {
		o = len(rs) - 1
	}

	ov := rs[o]
	return ov, o
}

func (rs runeOffsetSlice[T]) runeMark(target uint64) indexSet[uint64] {
	a, idx := rs.find(func(v indexSet[T]) bool {
		return uint64(v.r) >= target
	})
	if uint64(a.r) <= target || idx == 0 {
		return a.u64()
	}
	return rs[idx-1].u64()
}

func (rs runeOffsetSlice[T]) byteMark(target uint64) indexSet[uint64] {
	a, idx := rs.find(func(v indexSet[T]) bool {
		return uint64(v.b) >= target
	})
	if uint64(a.b) <= target || idx == 0 {
		return a.u64()
	}
	return rs[idx-1].u64()
}

func (rs runeOffsetSlice[T]) u16Mark(target uint64) indexSet[uint64] {
	a, idx := rs.find(func(v indexSet[T]) bool {
		return uint64(v.u16) >= target
	})
	if uint64(a.u16) <= target || idx == 0 {
		return a.u64()
	}
	return rs[idx-1].u64()
}
